#include <iostream>
#include <windows.h>

int main(int argc, char *argv[]) {
  std::cout << "Querying available displays..." << std::endl << std::endl;

  BOOL success;
  DISPLAY_DEVICE display;
  display.cb = sizeof(DISPLAY_DEVICE);

  int counter = 0;
  int monitorsFound = 0;

  while(success = EnumDisplayDevices(NULL, counter++, &display, 0))
  {
    std::cout << display.DeviceString << "(";

	DISPLAY_DEVICE screen;
	screen.cb = sizeof(DISPLAY_DEVICE);
	
	success = EnumDisplayDevices(display.DeviceName, 0, &screen, 0);

	if(success)
	{
	  ++monitorsFound;
      std::cout << screen.DeviceString;
	}
	else 
	{
		std::cout << "NULL";
	}
	std::cout << ")" << std::endl;
  }

  std::cout << std::endl << "Found a total of '" << monitorsFound << "' screens on '" << counter-1 << "' possible displays." << std::endl;

  return 0;
}